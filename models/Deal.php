<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use app\models\Lead;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 *
 * @property Lead $lead
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'leadId', 'name', 'amount'], 'required'],
            [['id', 'leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 55],
            [['leadId'], 'exist', 'skipOnError' => true, 'targetClass' => Lead::className(), 'targetAttribute' => ['leadId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
	///////////////////////4b//////////////////////////
	public function getDealItem()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadId']);
    }
	
	
	public static function getDeals()  
	{
		$allDeals = self::find()->all();
		$allDealsArray = ArrayHelper::
					map($allDeals, 'id', 'name');
		return $allDealsArray;
	}
	
	
	public function getLeads()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadId']);
    }
	///////////////////////4b//////////////////////////
}
